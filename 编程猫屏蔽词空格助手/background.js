chrome.contextMenus.create({
	title: '使用空格防屏蔽您的评论『%s』', // %s表示选中的文字
	contexts: ['selection'], // 只有当选中文字时才会出现此右键菜单
	onclick: function(params)
	{
		console.log(params);
		text=params.selectionText;
		var doneText=''
	    for(let char of text){
			console.log("字符："+char);
			doneText=doneText+char+' ';
		}
		var w = document.createElement('input');
		w.value = doneText;
		document.body.appendChild(w);
		w.select();
		// 调用浏览器的复制命令
		document.execCommand("Copy");
		// 将input元素隐藏，通知操作完成！
		w.style.display='none';
		chrome.notifications.create(null, {
			type: 'basic',
			iconUrl: 'icon.png',
			title: '编程猫反屏蔽词空格助手',
			message: '操作完成，请按下CTRL+V并发送！'
		});
	}
});